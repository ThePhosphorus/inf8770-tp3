# Réponses aux questions du TP3

Ce fichier viens en complement avec les fichiers de code.


## 1
**Question**: Identifiez les difficultés propres à la séquence vidéo anni009 pour son analyse.
Identifiez les problèmes que vous devrez résoudre.


## 2
**Question**: Proposez une méthode pour résoudre le problème. Donnez son algorithme général. Justifiez le choix de votre méthode en fonction des difficultés identifiés à
la question 1.

## 3
**Question**: Implantez la méthode proposée à la question 2. Vous devez obligatoirement intégrer dans votre code informatique des librairies de traitement d’images/vidéos
existantes (e.g. opencv). Avec un court texte, expliquez comment votre algorithme a été implanté, dans quel langage et avec quelle librairie.

## 4
**Question**: À partir du code informatique de la question 3, générez des résultats qui permettent d’évaluer la qualité de votre solution pour la séquence anni009. Utilisez
des graphiques ou des tableaux.

## 5
**Question**: Analysez en détail les performances de votre méthode à partir des résultats de
la question 4.


## 6
**Queation**: Sans modifier les paramètres de la méthode, comparez les résultats sur la séquence anni009 aux résultats que vous obtenez pour les séquences anni005 et
NAD57. Identifiez les limites de la solution proposée.